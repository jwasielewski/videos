package app.bear.videos;

import android.app.Application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.bear.videos.utils.PlaylistUtil;

public class VideosApplication extends Application {
    private static final Logger log = LoggerFactory.getLogger(VideosApplication.class);

    @Override
    public void onCreate() {
        log.trace("onCreate()");

        PlaylistUtil.instance().readConfiguration(this);
        PlaylistUtil.instance().validate(this);
    }

}
