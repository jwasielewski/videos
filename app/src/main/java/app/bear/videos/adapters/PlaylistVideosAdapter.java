package app.bear.videos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import app.bear.videos.R;
import app.bear.videos.utils.KeyUtil;
import butterknife.Bind;
import butterknife.ButterKnife;

public class PlaylistVideosAdapter extends RecyclerView.Adapter<PlaylistVideosAdapter.ViewHolder> {

    private WeakReference<Context> weakContext;
    private List<VideoData> videoDataList;

    public PlaylistVideosAdapter(Context context) {
        weakContext = new WeakReference<>(context);
        videoDataList = new ArrayList<>();
    }

    public void addNewItem(VideoData data) {
        videoDataList.add(data);
        notifyDataSetChanged();
    }

    public List<VideoData> getVideoDataList() {
        return videoDataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(weakContext.get()).inflate(R.layout.video_playlist_cell, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int postition) {
        Picasso.with(weakContext.get()).load(new File(videoDataList.get(postition).thumbnailPath)).fit()
                .noFade().into(viewHolder.thumbnail);

        viewHolder.path.setText(videoDataList.get(postition).filePath);

        SpinnerAdapter adapter = new ArrayAdapter<>(weakContext.get(), android.R.layout.simple_spinner_dropdown_item,
                KeyUtil.getSupportedCharacters());
        viewHolder.shortcut.setAdapter(adapter);

        if (videoDataList.get(postition).keyCode != -1) {
            viewHolder.shortcut.setSelection(videoDataList.get(postition).keyCode - KeyUtil.getCodeForPosition(0));
        }

        viewHolder.shortcut.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int selectedItemPosition, long id) {
                videoDataList.get(postition).keyCode = KeyUtil.getCodeForPosition(selectedItemPosition);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDialog.Builder builder = new MaterialDialog.Builder(weakContext.get());
                builder.content("Skasować ten element z playlisty?");
                builder.positiveText("Tak");
                builder.negativeText("Nie");
                builder.callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);

                        videoDataList.remove(postition);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoDataList != null ? videoDataList.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.vpc_thumbnail)
        ImageView thumbnail;

        @Bind(R.id.vpc_path)
        TextView path;

        @Bind(R.id.vpc_delete)
        ImageView delete;

        @Bind(R.id.vpc_shortcut)
        Spinner shortcut;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public static class VideoData {
        public String filePath;
        public String thumbnailPath;
        public int keyCode;

        public VideoData(String filePath, String thumbnailPath) {
            this.filePath = filePath;
            this.thumbnailPath = thumbnailPath;
            this.keyCode = -1;
        }

        @Override
        public String toString() {
            return "VideoData{" +
                    "filePath='" + filePath + '\'' +
                    ", thumbnailPath='" + thumbnailPath + '\'' +
                    ", keyCode=" + keyCode +
                    '}';
        }
    }

}
