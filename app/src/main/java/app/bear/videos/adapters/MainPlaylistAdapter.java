package app.bear.videos.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import app.bear.videos.R;
import app.bear.videos.activities.EditPlaylistActivity;
import app.bear.videos.activities.PlayerActivity;
import app.bear.videos.models.JsonPlaylistInfo;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainPlaylistAdapter extends RecyclerView.Adapter<MainPlaylistAdapter.PlaylistViewHolder> {

    private WeakReference<Context> weakContext;
    private List<JsonPlaylistInfo> playlistInfoList;

    public MainPlaylistAdapter(Context context, List<JsonPlaylistInfo> playlistInfoList) {
        weakContext = new WeakReference<>(context);
        if (playlistInfoList != null) {
            this.playlistInfoList = new ArrayList<>(playlistInfoList);
        }
    }

    public void setNewData(List<JsonPlaylistInfo> data) {
        if (data != null) {
            playlistInfoList = new ArrayList<>(data);
            notifyDataSetChanged();
        }
    }

    public void addAll(List<JsonPlaylistInfo> data) {
        if (data != null) {
            playlistInfoList.addAll(data);
            notifyDataSetChanged();
        }
    }

    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(weakContext.get()).inflate(R.layout.main_playlist_cell, viewGroup, false);
        return new PlaylistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlaylistViewHolder playlistViewHolder, final int position) {
        playlistViewHolder.name.setText(playlistInfoList.get(position).getName());

        playlistViewHolder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(weakContext.get(), PlayerActivity.class);
                intent.putExtra(PlayerActivity.PLAYLIST_ID, playlistInfoList.get(position).getId());
                weakContext.get().startActivity(intent);
            }
        });

        playlistViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(weakContext.get(), EditPlaylistActivity.class);
                intent.putExtra(EditPlaylistActivity.PLAYLIST_ID, playlistInfoList.get(position).getId());
                weakContext.get().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return playlistInfoList != null ? playlistInfoList.size() : 0;
    }

    public static class PlaylistViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.mpc_name)
        TextView name;

        @Bind(R.id.mpc_play)
        ImageView play;

        public PlaylistViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
