package app.bear.videos.activities;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import app.bear.videos.R;
import app.bear.videos.models.JsonPlaylist;
import app.bear.videos.models.JsonVideo;
import app.bear.videos.utils.KeyUtil;
import app.bear.videos.utils.PlaylistUtil;
import butterknife.Bind;
import butterknife.ButterKnife;

public class PlayerActivity extends Activity {
    private static final Logger log = LoggerFactory.getLogger(PlayerActivity.class);

    public static final String PLAYLIST_ID = "KEY_PLAYLIST_ID";
    private static final String CURRENT_VIDEO = "KEY_CURRENT_VIDEO";
    private static final String VIDEO_POSITION = "KEY_VIDEO_POSITION";

    @Bind(R.id.fullscreen_content)
    VideoView videoView;

    private int playlistId;
    private int currentVideo;
    private int videoPosition;
    private JsonPlaylist playlist;
    private MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_player);
        ButterKnife.bind(this);

        if (getActionBar() != null) {
            getActionBar().hide();
        }


        resotrePlaylistData(savedInstanceState);

        if (mediaController == null) {
            mediaController = new CustomMediaController(this);
        }

        videoView.setMediaController(mediaController);

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaController != null) {
                    mediaController.show(-1);
                }
            }
        });

        if (getIntent() != null && getIntent().hasExtra(PLAYLIST_ID)) {
            playlistId = getIntent().getIntExtra(PLAYLIST_ID, -1);

            playlist = PlaylistUtil.instance().getPlaylist(this, playlistId);

            if (playlist.getVideos().size() == 0) {
                Toast.makeText(this, "Playlista jest pusta!", Toast.LENGTH_LONG).show();
                finish();
            }
        } else {
            Toast.makeText(this, "Brak id playlisty!", Toast.LENGTH_LONG).show();
            finish();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        loadVideo(currentVideo);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                if (videoPosition == -1) {
                    videoView.seekTo(0);
                    videoView.start();
                } else {
                    videoView.seekTo(videoPosition);
                    videoView.pause();
                    videoPosition = -1;
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.reset();

                if (playlist != null && playlist.getVideos() != null) {
                    if (currentVideo + 1 == playlist.getVideos().size()) {
                        currentVideo = 0;
                    } else {
                        ++currentVideo;
                    }

                    loadVideo(currentVideo);
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putInt(CURRENT_VIDEO, currentVideo);
        savedInstanceState.putInt(VIDEO_POSITION, videoView.getCurrentPosition());
        savedInstanceState.putInt(PLAYLIST_ID, playlistId);

        videoView.pause();
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        resotrePlaylistData(savedInstanceState);
    }

    private void loadVideo(final int position) {
        try {
            videoView.setVideoPath(playlist.getVideos().get(position).getPath());
        } catch (Throwable t) {
            log.error(t.getMessage());
            t.printStackTrace();
        }
        videoView.start();
    }

    private void resotrePlaylistData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            videoPosition = -1;
            currentVideo = 0;
            return;
        }

        if (savedInstanceState.containsKey(CURRENT_VIDEO)) {
            currentVideo = savedInstanceState.getInt(CURRENT_VIDEO);
        } else {
            currentVideo = 0;
        }

        if (savedInstanceState.containsKey(VIDEO_POSITION)) {
            videoPosition = savedInstanceState.getInt(VIDEO_POSITION);
        } else {
            videoPosition = -1;
        }

        if (savedInstanceState.containsKey(PLAYLIST_ID)) {
            playlistId = savedInstanceState.getInt(PLAYLIST_ID);

            playlist = PlaylistUtil.instance().getPlaylist(this, playlistId);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, @NonNull KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_SPACE && videoView != null) {
            if (videoView.isPlaying()) {
                videoView.pause();
            } else {
                videoView.start();
            }
            return true;
        }

        log.debug("onKeyUp:'{}'", keyCode);
        if (KeyUtil.isKeyCodeSupported(keyCode)) {
            List<JsonVideo> videos = playlist.getVideos();
            for (int i = 0; i < videos.size(); ++i) {
                log.debug("'{}' => '{}'", i, videos.get(i).getKeyCode());
                if (videos.get(i).getKeyCode() == keyCode) {
                    currentVideo = i;
                    loadVideo(currentVideo);
                    return true;
                }
            }
        }

        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CustomMediaController extends MediaController {

        public CustomMediaController(Context context) {
            super(context);
        }

        public CustomMediaController(Context context, boolean useFastForward) {
            super(context, useFastForward);
        }

        public CustomMediaController(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public void show(int timeout) {
            // przy przełączaniu filmików pokazywane są kontrolki
            // a to jest jeden ze sposobów aby temu zaradzić
            // ważne: umożliwić pokazanie kontrolek na 3 sek
            if (timeout == -1) {
                super.show(3000);
            }
        }
    }
}
