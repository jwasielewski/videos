package app.bear.videos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.bear.videos.R;
import app.bear.videos.adapters.MainPlaylistAdapter;
import app.bear.videos.utils.PlaylistUtil;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final Logger log = LoggerFactory.getLogger(MainActivity.class);

    @Bind(R.id.main_toolbar)
    Toolbar toolbar;

    @Bind(R.id.main_playlists)
    RecyclerView playlists;

    @Bind(R.id.main_fab)
    FloatingActionButton fab;

    private MainPlaylistAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        log.trace("onCreate('{}')", savedInstanceState);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        playlists.setLayoutManager(linearLayoutManager);

        adapter = new MainPlaylistAdapter(this, PlaylistUtil.instance().getPlaylistsInfo());
        playlists.setAdapter(adapter);

        fab.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (adapter != null) {
            adapter.setNewData(PlaylistUtil.instance().getPlaylistsInfo());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        log.trace("onClick('{}')", v);

        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.dialog_new_playlist, null);
        final MaterialEditText name = (MaterialEditText) linearLayout.findViewById(R.id.dialog_new_playlist_name);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.customView(linearLayout, false);
        builder.positiveText(getResources().getString(R.string.ok));
        builder.neutralText(getResources().getString(R.string.cancel));

        builder.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                String pname = name.getText().toString().trim();
                if (pname.length() == 0) {
                    Snackbar.make((View) fab.getParent(), "Nazwa nie może być pusta!", Snackbar.LENGTH_SHORT).show();
                    return;
                } else {
                    super.onPositive(dialog);
                }

                Intent intent = new Intent(MainActivity.this, EditPlaylistActivity.class);
                intent.putExtra(EditPlaylistActivity.PLAYLIST_NAME, pname);
                MainActivity.this.startActivity(intent);
            }

            @Override
            public void onNegative(MaterialDialog dialog) {
                super.onNegative(dialog);
            }
        });

        builder.show();
    }
}
