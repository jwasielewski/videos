package app.bear.videos.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import app.bear.videos.R;
import app.bear.videos.adapters.PlaylistVideosAdapter;
import app.bear.videos.models.JsonPlaylist;
import app.bear.videos.models.JsonVideo;
import app.bear.videos.utils.PlaylistUtil;
import app.bear.videos.utils.VideoUtils;
import butterknife.Bind;
import butterknife.ButterKnife;

public class EditPlaylistActivity extends AppCompatActivity {
    private static final Logger log = LoggerFactory.getLogger(EditPlaylistActivity.class);

    public static final String PLAYLIST_NAME = "KEY_PLAYLIST_NAME";
    public static final String PLAYLIST_ID = "KEY_PLAYLIST_ID";
    private static final int PICK_CODE = 666;

    @Bind(R.id.epa_toolbar)
    Toolbar toolbar;

    @Bind(R.id.epa_playlists)
    RecyclerView playlistView;

    @Bind(R.id.epa_fab)
    FloatingActionButton fab;

    private boolean editMode;
    private int playlistId;
    private String playlistName;
    private PlaylistVideosAdapter adapter;
    private JsonPlaylist playlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_playlist);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        editMode = false;

        if (getIntent() != null && (getIntent().hasExtra(PLAYLIST_NAME) || getIntent().hasExtra(PLAYLIST_ID))) {
            if (getIntent().hasExtra(PLAYLIST_NAME)) {
                playlistName = getIntent().getStringExtra(PLAYLIST_NAME);
                editMode = false;
            } else if (getIntent().hasExtra(PLAYLIST_ID)) {
                playlistId = getIntent().getIntExtra(PLAYLIST_ID, -1);
                if (playlistId != -1) {
                    playlist = PlaylistUtil.instance().getPlaylist(this, playlistId);
                    playlistName = playlist.getName();
                    editMode = true;
                }
            }

            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(playlistName);
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.edit_playlist_no_name_error), Toast.LENGTH_LONG)
                    .show();
            finish();

            return;
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        playlistView.setLayoutManager(linearLayoutManager);

        adapter = new PlaylistVideosAdapter(this);
        playlistView.setAdapter(adapter);

        if (editMode) {
            for (JsonVideo video : playlist.getVideos()) {
                PlaylistVideosAdapter.VideoData data = new PlaylistVideosAdapter.VideoData(
                        video.getPath(),
                        video.getThumbnail()
                );
                data.keyCode = video.getKeyCode();

                adapter.addNewItem(data);
            }
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("video/*");
                startActivityForResult(intent, PICK_CODE);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_CODE) {
            log.debug("'{}'", VideoUtils.getRealPathFromURI(this, data.getData()));

            adapter.addNewItem(new PlaylistVideosAdapter.VideoData(
                    VideoUtils.getRealPathFromURI(this, data.getData()),
                    VideoUtils.getThumbnailPathForLocalFile(this, data.getData())
            ));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_playlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_mep_delete) {
            if (editMode) {
                PlaylistUtil.instance().removePlaylistFromConfiguration(playlistId);
                PlaylistUtil.instance().storeConfiguration(this);
            }
            finish();
            return true;
        }

        if (id == R.id.action_mep_save) {
            if (validatePlaylistData()) {
                savePlaylist(adapter.getVideoDataList());
                finish();
            } else {
                MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
                builder.title("Błąd");
                builder.content("Playlista zawiera powtarzające się skróty klawiszowe!");
                builder.positiveText("OK");
                builder.show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.content("Czy na pewno chcesz porzucić wprowadzone zmiany?");
        builder.positiveText("Tak");
        builder.negativeText("Nie");
        builder.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);

                EditPlaylistActivity.super.onBackPressed();
            }

            @Override
            public void onNegative(MaterialDialog dialog) {
                super.onNegative(dialog);
            }
        });

        builder.show();
    }

    private boolean validatePlaylistData() {
        List<PlaylistVideosAdapter.VideoData> dataList = adapter.getVideoDataList();
        Set<Integer> checkDuplicatedKeyCodes = new HashSet<>();

        for (PlaylistVideosAdapter.VideoData data : dataList) {
            checkDuplicatedKeyCodes.add(data.keyCode);
        }

        return dataList.size() == checkDuplicatedKeyCodes.size();
    }

    private void savePlaylist(List<PlaylistVideosAdapter.VideoData> data) {
        JsonPlaylist jsonPlaylist = new JsonPlaylist();
        if (editMode) {
            jsonPlaylist.setId(playlistId);
        } else {
            jsonPlaylist.setId(PlaylistUtil.instance().getIdForNewPlaylist());
        }
        jsonPlaylist.setName(playlistName);

        List<JsonVideo> videos = new ArrayList<>();
        for (PlaylistVideosAdapter.VideoData item : data) {
            JsonVideo video = new JsonVideo();
            video.setPath(item.filePath);
            video.setThumbnail(item.thumbnailPath);
            video.setKeyCode(item.keyCode);

            videos.add(video);
        }

        jsonPlaylist.setVideos(videos);

        PlaylistUtil.instance().storePlaylist(this, jsonPlaylist);
        if (!editMode) {
            PlaylistUtil.instance().addPlaylistToConfiguration(jsonPlaylist);
        }
        PlaylistUtil.instance().storeConfiguration(this);
    }

}
