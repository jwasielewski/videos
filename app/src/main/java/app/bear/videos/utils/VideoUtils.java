package app.bear.videos.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VideoUtils {
    private static final Logger log = LoggerFactory.getLogger(VideoUtils.class);

    private static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    private static String[] mediaColumns = {MediaStore.Video.Media._ID};

    private VideoUtils() {

    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(columnIndex);
        cursor.close();
        return path;
    }

    public static String getThumbnailPathForLocalFile(Activity context, Uri fileUri) {

        long fileId = getFileId(context, fileUri);

        MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(),
                fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);

        Cursor thumbCursor = null;
        try {
            thumbCursor = context.managedQuery(
                    MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                    thumbColumns, MediaStore.Video.Thumbnails.VIDEO_ID + " = " + fileId, null, null
            );

            if (thumbCursor.moveToFirst()) {
                return thumbCursor.getString(thumbCursor .getColumnIndex(MediaStore.Video.Thumbnails.DATA));
            }

        } catch (Throwable t) {
            log.error(t.getMessage());
        }

        return null;
    }

    public static long getFileId(Activity context, Uri fileUri) {
        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null, null);

        if (cursor.moveToFirst()) {
            int columnIndex = cursor .getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            return cursor.getInt(columnIndex);
        }

        return 0;
    }

}
