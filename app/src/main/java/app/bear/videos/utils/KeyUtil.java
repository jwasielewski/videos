package app.bear.videos.utils;

import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class KeyUtil {

    private static List<Pair<Integer, String>> supportedKeys;

    static {
        supportedKeys = new ArrayList<>();
        supportedKeys.add(new Pair<>(29, "a"));
        supportedKeys.add(new Pair<>(30, "b"));
        supportedKeys.add(new Pair<>(31, "c"));
        supportedKeys.add(new Pair<>(32, "d"));
        supportedKeys.add(new Pair<>(33, "e"));
        supportedKeys.add(new Pair<>(34, "f"));
        supportedKeys.add(new Pair<>(35, "g"));
        supportedKeys.add(new Pair<>(36, "h"));
        supportedKeys.add(new Pair<>(37, "i"));
        supportedKeys.add(new Pair<>(38, "j"));
        supportedKeys.add(new Pair<>(39, "k"));
        supportedKeys.add(new Pair<>(40, "l"));
        supportedKeys.add(new Pair<>(41, "m"));
        supportedKeys.add(new Pair<>(42, "n"));
        supportedKeys.add(new Pair<>(43, "o"));
        supportedKeys.add(new Pair<>(44, "p"));
        supportedKeys.add(new Pair<>(45, "q"));
        supportedKeys.add(new Pair<>(46, "r"));
        supportedKeys.add(new Pair<>(47, "s"));
        supportedKeys.add(new Pair<>(48, "t"));
        supportedKeys.add(new Pair<>(49, "u"));
        supportedKeys.add(new Pair<>(50, "v"));
        supportedKeys.add(new Pair<>(51, "w"));
        supportedKeys.add(new Pair<>(52, "x"));
        supportedKeys.add(new Pair<>(53, "y"));
        supportedKeys.add(new Pair<>(54, "z"));
        supportedKeys.add(new Pair<>(7, "0"));
        supportedKeys.add(new Pair<>(8, "1"));
        supportedKeys.add(new Pair<>(9, "2"));
        supportedKeys.add(new Pair<>(10, "3"));
        supportedKeys.add(new Pair<>(11, "4"));
        supportedKeys.add(new Pair<>(12, "5"));
        supportedKeys.add(new Pair<>(13, "6"));
        supportedKeys.add(new Pair<>(14, "7"));
        supportedKeys.add(new Pair<>(15, "8"));
        supportedKeys.add(new Pair<>(16, "9"));
        supportedKeys.add(new Pair<>(131, "F1"));
        supportedKeys.add(new Pair<>(132, "F2"));
        supportedKeys.add(new Pair<>(133, "F3"));
        supportedKeys.add(new Pair<>(134, "F4"));
        supportedKeys.add(new Pair<>(135, "F5"));
        supportedKeys.add(new Pair<>(136, "F6"));
        supportedKeys.add(new Pair<>(137, "F7"));
        supportedKeys.add(new Pair<>(138, "F8"));
        supportedKeys.add(new Pair<>(139, "F9"));
        supportedKeys.add(new Pair<>(140, "F10"));
        supportedKeys.add(new Pair<>(141, "F11"));
        supportedKeys.add(new Pair<>(142, "F12"));
    }

    private KeyUtil() {

    }

    public static List<String> getSupportedCharacters() {
        List<String> result = new ArrayList<>();
        for (Pair<Integer, String> pair : supportedKeys) {
            result.add(pair.second);
        }
        return result;
    }

    public static int getCodeForPosition(int position) {
        return supportedKeys.get(position).first;
    }

    public static boolean isKeyCodeSupported(int keyCode) {
        for (Pair<Integer, String> pair : supportedKeys) {
            if (pair.first == keyCode) {
                return true;
            }
        }
        return false;
    }

}
