package app.bear.videos.utils;

import android.content.Context;

import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import app.bear.videos.models.JsonConfiguration;
import app.bear.videos.models.JsonPlaylist;
import app.bear.videos.models.JsonPlaylistInfo;

public class PlaylistUtil {
    private static final Logger log = LoggerFactory.getLogger(PlaylistUtil.class);

    private static final String PLAYLISTS_DIR = "";
    private static final String CONFIGURATION_FILE = "configuration.json";
    private static PlaylistUtil _instance;
    private JsonConfiguration configuration;
    private Gson gson;

    private PlaylistUtil() {
        log.trace("PlaylistUtil new instance");

        gson = new Gson();
    }

    public static PlaylistUtil instance() {
        log.trace("instance()");

        if (_instance == null) {
            _instance = new PlaylistUtil();
        }
        return _instance;
    }

    public void validate(Context context) {
        log.trace("validate('{}')", context);

        if (configuration == null) {
            configuration = new JsonConfiguration();
            configuration.setPlaylists(new ArrayList<JsonPlaylistInfo>());
            storeConfiguration(context);
        }
    }

    public boolean readConfiguration(Context context) {
        log.trace("readConfiguration('{}')", context);

        try {
            InputStreamReader stream = new InputStreamReader(context.openFileInput(CONFIGURATION_FILE));
            BufferedReader reader = new BufferedReader(stream);

            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            reader.close();
            stream.close();

            configuration = gson.fromJson(builder.toString(), JsonConfiguration.class);

            return true;
        } catch (Throwable t) {
            log.error(t.getMessage());
        }

        return false;
    }

    public boolean storeConfiguration(Context context) {
        log.trace("storeConfiguration('{}')", context);

        try {
            OutputStreamWriter writer = new OutputStreamWriter(context.openFileOutput(CONFIGURATION_FILE, Context
                    .MODE_PRIVATE));
            writer.write(gson.toJson(configuration));
            writer.flush();
            writer.close();

            return true;
        } catch (Throwable t) {
            log.error(t.getMessage());
        }

        return false;
    }

    public List<JsonPlaylistInfo> getPlaylistsInfo() {
        log.trace("getPlaylistsInfo()");

        return new ArrayList<>(configuration.getPlaylists());
    }

    public void removePlaylistFromConfiguration(int id) {
        List<JsonPlaylistInfo> configurationPlaylists = configuration.getPlaylists();
        for (int i = 0; i < configurationPlaylists.size(); ++i) {
            if (configurationPlaylists.get(i).getId() == id) {
                configurationPlaylists.remove(i);
                configuration.setPlaylists(configurationPlaylists);
                return;
            }
        }
    }

    public int getIdForNewPlaylist() {
        log.trace("getIdForNewPlaylist()");

        int nextId = -1;

        for (JsonPlaylistInfo playlistInfo : configuration.getPlaylists()) {
            if (playlistInfo.getId() > nextId) {
                nextId = playlistInfo.getId();
            }
        }

        return ++nextId;
    }

    public JsonPlaylist getPlaylist(Context context, int id) {
        log.trace("getPlaylist('{}', '{}')", context, id);

        try {
            InputStreamReader stream = new InputStreamReader(context.openFileInput(PLAYLISTS_DIR + id + ".json"));
            BufferedReader reader = new BufferedReader(stream);

            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            reader.close();
            stream.close();

            return gson.fromJson(builder.toString(), JsonPlaylist.class);
        } catch (Throwable t) {
            log.error(t.getMessage());
        }

        return null;
    }

    public void addPlaylistToConfiguration(JsonPlaylist playlist) {
        log.trace("addPlaylistToConfiguration('{}')", playlist);

        JsonPlaylistInfo playlistInfo = new JsonPlaylistInfo();
        playlistInfo.setId(playlist.getId());
        playlistInfo.setName(playlist.getName());
        configuration.getPlaylists().add(playlistInfo);
    }

    public boolean storePlaylist(Context context, JsonPlaylist playlist) {
        log.trace("storePlaylist('{}', '{}')", context, playlist);

        try {
            OutputStreamWriter writer = new OutputStreamWriter(context.openFileOutput(PLAYLISTS_DIR +
                    playlist.getId() + ".json", Context.MODE_PRIVATE));
            writer.write(gson.toJson(playlist));
            writer.flush();
            writer.close();

            return true;
        } catch (Throwable t) {
            log.error(t.getMessage());
        }

        return false;
    }

}
