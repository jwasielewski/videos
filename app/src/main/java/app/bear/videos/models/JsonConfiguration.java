package app.bear.videos.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JsonConfiguration {

    @SerializedName("playlists")
    private List<JsonPlaylistInfo> playlists;

    public List<JsonPlaylistInfo> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<JsonPlaylistInfo> playlists) {
        this.playlists = playlists;
    }
}
