package app.bear.videos.models;

import android.support.v4.util.Pair;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JsonPlaylist {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("videos")
    private List<JsonVideo> videos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JsonVideo> getVideos() {
        return videos;
    }

    public void setVideos(List<JsonVideo> videos) {
        this.videos = videos;
    }
}
