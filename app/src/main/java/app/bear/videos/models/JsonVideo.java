package app.bear.videos.models;

import com.google.gson.annotations.SerializedName;

public class JsonVideo {

    @SerializedName("path")
    private String path;

    @SerializedName("key_code")
    private int keyCode;

    @SerializedName("thumbnail")
    private String thumbnail;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(int keyCode) {
        this.keyCode = keyCode;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
